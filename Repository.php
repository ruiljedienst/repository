<?php
/**
 * @author: Patrick vd Pols
 * @date: 2019-10-11
 * @time: 15:39
 */

namespace Toscani\Repository;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Application;

/**
 * Class Repository
 * @package App\Repositories
 */
abstract class Repository implements RepositoryInterface
{
    /**
     * @var Application
     */
    protected $application;

    /**
     * @var Model
     */
    protected $model;

    /**
     * Repository constructor.
     * @param Application $application
     */
    public function __construct(Application $application)
    {
        $this->application = $application;

        $this->makeModel();
    }

    /**
     * Creates a model instance
     *
     * @return void
     */
    protected function makeModel()
    {
        if(!isset($this->modelClass) || !$this->modelClass){
            throw new \LogicException( get_class($this) . ' must have a property $modelClass');
        }

        $this->model = $this->application->make($this->modelClass);
    }

}